# WAES Assignment for QA specialists

Thank you for your interest in WAES and for taking the time to complete QA challenge.
We love to push techology further to change life for the better. 
And quality assurance is important part of this changes.

## Goal

The goal of this assignment is to check your skills in mobile testing.
We evaluate the assignment depending on your level of seniority regarding coverage, strategy and presentation.

## Task

Just imagine you work on new project which is a mobile application.
Let's say it is a calculator application.

Your tasks are:

1. Create automation framework from the scratch for the native Android calculator.
2. The test should verify next features:
	- basic arithmetic operations: addition (+), subtraction (-), multiplication (*), and division (/).
	- sine calculation - sin()
	- calculation history

## Additional requirements

- Your project should cover as many functional scenarios as you judge necessary for the given features and the test data available.
- Pay attention that native calculator could be different and depends on Android OS version.
- The code should be well designed / easily maintained / the test’s execution should be easily managed

## Technologies

The assignment can be developed with any of three most popular languages Java/JS/Python and any testing framework.
Choose your weapon of choice.

## Submission Guidelines

- Report with test results should be automatically generated.
- Include a `README.md` with instructions how to run tests, together with any additional information you consider appropriate (assumptions, design decisions made, etc.)

## Nice to have

- Support of different Android OS versions.
- A report with all relevant information if any defects are found.


#### Please upload the assignment to your personal GitHub/Bitbucket account once finished and send the link to the responsible person in WAES before deadline.